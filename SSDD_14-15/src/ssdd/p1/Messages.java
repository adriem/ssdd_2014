/*
 * AUTOR: Adrián Moreno Jimeno
 * NIA: 631537
 * FICHERO: Messages.java
 * TIEMPO: 10-15 min.
 * DESCRIPCION: clase que elige el mensaje a enviar al cliente
 */

package ssdd.p1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Messages {
	public static String errorResponse (int code){
		String html="";
		String response="";
		switch(code){
		case 400:
			response=String.format("HTTP/1.1 400 BAD REQUEST%n");
			html=String.format("<html><head>%n<title>400 Bad Request</title>%n" +
					"</head><body>%n<h1>Bad Request</h1>%n</body></html>");
			break;
		case 403:
			response=String.format("HTTP/1.1 403 FORBIDDEN%n");
			html=String.format("<html><head>%n<title>403 Forbidden</title>%n" +
					"</head><body>%n<h1>Forbidden</h1>%n</body></html>");
			break;
		case 404:
			response=String.format("HTTP/1.1 404 NOT FOUND%n");
			html=String.format("<html><head>%n<title>404 Not Found</title>%n" +
					"</head><body>%n<h1>Not Found</h1>%n</body></html>");
			break;
		case 501:
			response=String.format("HTTP/1.1 501 NOT IMPLEMENTED%n");
			html=String.format("<html><head>%n<title>501 Not Implemented</title>%n" +
					"</head><body>%n<h1>Not Implemented</h1>%n</body></html>");
			break;
			
				
		}
		response += String.format("Content-Type: text/html%n");
		response += String.format("Content-Lenght: %d%n%n%s", html.length(), html);
		return response;
	}
	
	public static String getResponse (File file) throws FileNotFoundException{
		String response="";
		response += String.format("HTTP/1.1 200 OK%n");
		response += String.format("Content-Type: text/html%n");
		Scanner scanner = new Scanner(file);
		response += String.format("Content-Lenght: %d%n%n", file.length());
		while (scanner.hasNextLine())
			response += scanner.nextLine();
		scanner.close();
		return response;
	}
	
	public static String postConfirmation (String fileName, String fileBody) {
		String html=String.format("<html><head>%n<title>��xito!</title>%n</head><body>%n");
		html += String.format("<h1>��xito!</h1>%n<p>Se ha escrito lo siguiente en el fichero %s:</p>%n", fileName);
		html += String.format("<pre>%n%s%n</pre>%n</body></html>", fileBody);
		String response="";
		response += String.format("HTTP/1.1 200 Ok%n");
		response += String.format("Content-Type: text/html%n");
		response += String.format("Content-Lenght: %d%n%n%s", html.length(), html);;
		return response;
	}
}

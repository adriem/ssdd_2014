/*
 * AUTOR: Adrián Moreno Jimeno
 * NIA: 631537
 * FICHERO: ServerThread.java
 * TIEMPO: 5 h.
 * DESCRIPCION: threads que crea el servidor bloqueante
 */

package ssdd.p1.threads;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URLDecoder;
import java.util.Formatter;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ssdd.p1.BlockingHTTPParser;
import ssdd.p1.Messages;

public class ServerThread implements Runnable{
	
	Socket socket;
	
	public ServerThread (Socket s) {
		socket = s;
	}
	
	public void run () {
		try{
			InputStream input = socket.getInputStream();
			OutputStream output = socket.getOutputStream();
			BlockingHTTPParser parser = new BlockingHTTPParser();
			File requestedFile;

			parser.parseRequest(input); //Llamada bloqueante
			
			if (parser.failed()) {
				//400 ROUTINE
				output.write(Messages.errorResponse(400).getBytes());
			}
			else if (parser.isComplete()) {
				if (parser.getMethod().equals("GET")) {
					//GET ROUTINE
					requestedFile = new File(System.getProperty("user.dir") + parser.getPath());
					if (!requestedFile.isFile()){
						output.write(Messages.errorResponse(404).getBytes());
					}
					else if (requestedFile.getParentFile().getAbsolutePath().equals(System.getProperty("user.dir"))) {
						output.write(Messages.getResponse(requestedFile).getBytes());
					}
					else {
						output.write(Messages.errorResponse(403).getBytes());
					}
				}
				else if (parser.getMethod().equals("POST")) {
					//POST ROUTINE
					String body = URLDecoder.decode(new String(parser.getBody().array()), "UTF-8");
					/*Pattern bodyPattern = Pattern.compile("\\s*fname=(\\w+[\\.\\w+])&content=(.*)");
					Matcher bodyMatcher = bodyPattern.matcher(body);*/
					requestedFile = new File(System.getProperty("user.dir") + "/" + body.substring(6, body.indexOf("&")));//bodyMatcher.group(1));
					if (!requestedFile.isFile()){
						output.write(Messages.errorResponse(404).getBytes());
					}
					else if(requestedFile.getParentFile().getAbsolutePath().equals(System.getProperty("user.dir"))) {
						Formatter formatter = new Formatter(requestedFile);
						formatter.format(body.substring(body.indexOf("&")+9));//bodyMatcher.group(2));
						formatter.close();
						output.write(Messages.postConfirmation(body.substring(6, body.indexOf("&")), body.substring(body.indexOf("&")+9)).getBytes()); //bodyMatcher.group(1),bodyMatcher.group(2)).getBytes());
					}
					else {
						output.write(Messages.errorResponse(403).getBytes());
					}
				}
				else {
					//501 ERROR ROUTINE
					output.write(Messages.errorResponse(501).getBytes());
				}
			}
			input.close();
			output.flush();
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

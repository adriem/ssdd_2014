/*
 * AUTOR: Adrián Moreno Jimeno
 * NIA: 631537
 * FICHERO: Server.java
 * TIEMPO: 5 min.
 * DESCRIPCION: servidor bloqueante
 */

package ssdd.p1.threads;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	public final int PORT;
	ServerSocket serverSocket;
	Socket clientSocket;
	
	public Server (int port) {
		PORT = port;
	}
	
	public void loop () {
		try {
			serverSocket = new ServerSocket(PORT);
			while (true) {
				try{
					clientSocket = serverSocket.accept();
					Thread process = new Thread(new ServerThread(clientSocket));
					process.start();
				} catch (IOException e) {
					System.err.println("Accept failed.");
					e.printStackTrace();
				}
			}
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

/*
 * AUTOR: Adrián Moreno Jimeno
 * NIA: 631537
 * FICHERO: Server.java
 * TIEMPO: 3 h.
 * DESCRIPCION: servidor no bloqueante
 */

package ssdd.p1.selector;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Formatter;
import java.util.Iterator;
import java.util.Set;

import ssdd.p1.HTTPParser;
import ssdd.p1.Messages;

public class Server {
	public final int PORT;
	Selector selector;
	ServerSocketChannel serverSocketChannel;
	File requestedFile;
	
	public Server (int port){
		PORT = port;
	}
	
	public void loop () {
		try {
			selector = Selector.open();
			serverSocketChannel = ServerSocketChannel.open();
			serverSocketChannel.socket().bind(new InetSocketAddress (PORT)); //Hacemos bind manualmente
			serverSocketChannel.configureBlocking(false); //Establecemos el canal como no bloqueante;
			serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT); //Registramos el canal en el seletor para que reciba conexiones entrantes
			while (true) {
				selector.select(); //Esta llamada se bloquea cuando no ha habido cambios en el selector
				Set<SelectionKey> selectedKeys = selector.selectedKeys(); //Obtenemos qué canales están listos
				Iterator<SelectionKey> keysIterator = selectedKeys.iterator();
				while (keysIterator.hasNext()) {
					SelectionKey key = keysIterator.next();
					if (key.isAcceptable()) {
						SocketChannel newClient = ((ServerSocketChannel) key.channel()).accept(); //Creamos el socket con el cliente
						newClient.configureBlocking(false);
						newClient.register(selector, SelectionKey.OP_READ, new HTTPParser());
					}
					else if (key.isReadable()) {
						ByteBuffer buffer = ByteBuffer.allocate(512); //Creamos el buffer
						SocketChannel channel = (SocketChannel) key.channel();
						HTTPParser parser = (HTTPParser) key.attachment();
						int bytesRead = channel.read(buffer); //Cargamos el buffer
						while (bytesRead > 0) {
							buffer.flip(); //Ponemos buffer en modo lectura
							parser.parseRequest(buffer); //Añadimos al parser lo que hay en el buffer y parseamos
							buffer.clear();
							bytesRead = channel.read(buffer);
						}	
						
						String msg = null;
						
						if (parser.isComplete()) {
							if (parser.getMethod().equals("GET")) {
								//GET ROUTINE
								requestedFile = new File(System.getProperty("user.dir") + parser.getPath());
								if (!requestedFile.isFile()){
									msg = Messages.errorResponse(404);
								}
								else if (requestedFile.getParentFile().getAbsolutePath().equals(System.getProperty("user.dir"))) {
									msg = Messages.getResponse(requestedFile);
								}
								else {
									msg = Messages.errorResponse(403);
								}
							}
							else if (parser.getMethod().equals("POST")) {
								//POST ROUTINE
								String body = URLDecoder.decode(new String(parser.getBody().array()), "UTF-8");
								System.out.println(body);
								requestedFile = new File(System.getProperty("user.dir") + "/" + body.substring(6, body.indexOf("&")));//bodyMatcher.group(1));
								if (!requestedFile.isFile()){
									msg = Messages.errorResponse(404);
								}
								else if(requestedFile.getParentFile().getAbsolutePath().equals(System.getProperty("user.dir"))) {
									Formatter formatter = new Formatter(requestedFile);
									formatter.format(body.substring(body.indexOf("&")+9));//bodyMatcher.group(2));
									formatter.close();
									msg = Messages.postConfirmation(body.substring(6, body.indexOf("&")), body.substring(body.indexOf("&")+9));
								}
								else {
									msg = Messages.errorResponse(403);
								}
							}
							else {
								//501 ERROR ROUTINE
								msg = Messages.errorResponse(501);
							}
						}
						else if (parser.failed()) {
							//400 ROUTINE
							msg = Messages.errorResponse(400);
						}
						if (msg != null) {
							key.interestOps(SelectionKey.OP_WRITE); //Indicamos que queremos usar este canal para escribir
							buffer = ByteBuffer.allocate(msg.length()+1);
							buffer.put(msg.getBytes());
							buffer.flip();
							key.attach(buffer); //Guardamos el buffer listo para ser escrito en el canal
						}
					}
					else if (key.isWritable()){
						ByteBuffer buffer = (ByteBuffer) key.attachment();
						SocketChannel channel = (SocketChannel) key.channel();
						channel.write(buffer);
						if (buffer.remaining() == 0){
							channel.close();
							key.cancel();
						}
						
					}
					keysIterator.remove();
				}
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

/*
 * AUTOR: Adrián Moreno Jimeno
 * NIA: 631537
 * FICHERO: Main.java
 * TIEMPO: 5 min.
 * DESCRIPCION: clase principal que elige el servidor y el puerto por parametros
 */

package ssdd.p1;

public class Main {
	public static void main (String [] args) {
		if(args.length<2 || (!args[0].equals("-t") && !args[0].equals("-s"))) 
				System.out.print("Incorrect arguments. Usage: java -jar NIP_SSDDp1.jar -t|-s puerto");
		else if (args[0].equals("-t")) {
			ssdd.p1.threads.Server server = new ssdd.p1.threads.Server(Integer.parseInt(args[1]));
			server.loop();
		}
		else if (args[0].equals("-s")) {
			ssdd.p1.selector.Server server = new ssdd.p1.selector.Server(Integer.parseInt(args[1]));
			server.loop();
		}
	}
}

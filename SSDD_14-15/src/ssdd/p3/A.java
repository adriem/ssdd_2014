package ssdd.p3;

import java.io.FileNotFoundException;

import ssdd.ms.old.MessageSystem;

public class A {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		boolean debug = false;
		String networkFile = "E:\\Adrian\\Java\\ssdd_2014\\SSDD_14-15\\src\\ssdd\\p3\\peers.txt";
		
		for (String arg : args) {
			if (arg.equals("-d"))
				debug = true;
			else
				networkFile = arg;
		}
		
		try {
			MessageSystem ms = new MessageSystem(1, networkFile, debug);
			ms.send(3, new MessageValue(1));
			ms.stopMailbox();
		} catch (FileNotFoundException e) {
			System.err.println("El fichero " + networkFile + " no existe.");
		}
	}

}
/*
 * AUTOR: Adri�n Moreno Jimeno
 * NIA: 631537
 * FICHERO: MassageSystem.java
 * TIEMPO: 4 horas
 * DESCRIPCI�N: 
 *   Este fichero contiene la clase ChatApplication, que contiene el m�todo main
 *   y es la encargada de inicializar todos los threads de la aplicaci�n
 */
package ssdd.p4;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.concurrent.ArrayBlockingQueue;

import javax.swing.JFrame;

import ssdd.ms.Envelope;
import ssdd.ms.MessageSystem;
import ssdd.ms.TotalOrderMulticast;

/**
 * Clase que contiene el m�todo main y que se encarga de ejecutar la aplicaci�n
 * de chat.
 * 
 * @author Adrian Moreno
 * @version 1.0 27 Nov 2014
 */
public class ChatApplication {
	
	public static final int MSG_OUTBOX_SIZE = 5;
	public static final int MSG_INBOX_SIZE = 100;
	
	private ArrayBlockingQueue<String> outbox;
	private MessageSystem ms;
	private TotalOrderMulticast tom;
	private ChatDialog dialog;
	private Thread collector;//Thread que recoge y envia los mensajes de outbox
	
	/**
	 * Crea una instancia de ChatApplication inicianiz�ndola con los valores
	 * que se le pasan
	 * 
	 * @param nProc Proceso al que pertenece el chat
	 * @param networkFile Ruta del archivo con informaci�n de los procesos
	 * @param debug Flag que indica si hay que mostrar los mensajes de debug 
	 */
	public ChatApplication(int nProc, String networkFile, boolean debug) 
			throws FileNotFoundException {
		outbox = new ArrayBlockingQueue<String>(MSG_OUTBOX_SIZE);
		ms = new MessageSystem(nProc, networkFile, debug, MSG_INBOX_SIZE);
		tom = new TotalOrderMulticast(ms);
		dialog = new ChatDialog(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				String m = dialog.text();
				if(!m.isEmpty() && outbox.remainingCapacity() > 0){
					try{
						outbox.put(m);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});
		dialog.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		collector = new Thread() {
			@Override
			public void run() {
				while (dialog.isVisible()) {
					try{
						tom.sendMulticast(outbox.take());
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};
	}
	
	/**
	 * Arranca todos los threads de la aplicaci�n de chat
	 */
	public void runChat() {
		dialog.setVisible(true);
		collector.start();	
		while (dialog.isVisible()) {
			Envelope received = tom.receiveMulticast();
			if (received.getSource() == ms.getSource()) {
				dialog.addMessage("Yo: " + received.getPayload());
			} else {
				dialog.addMessage(received.getSource() + ": " 
						+ received.getPayload());
			}
		}
		ms.stopMailbox();
	}
	
	public static void main (String [] args) {
		try {
			int debugParamOffset = 0;
			boolean debug = false;
			if (args.length == 3) {
				debugParamOffset = 1;
			}
			if (args.length == 3 && args[0].equals("-d")) {
				debug = true;
			}
			
			ChatApplication chat = new ChatApplication(
					Integer.parseInt(args[0+debugParamOffset]),
					args[1+debugParamOffset], debug);
			chat.runChat();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}

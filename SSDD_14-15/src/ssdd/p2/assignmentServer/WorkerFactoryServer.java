/*
 * AUTOR: Adri�n Moreno Jimeno
 * NIA: 631537
 * FICHERO: WorkerFactoryServer.java
 * TIEMPO: 1,5 horas
 * DESCRIPCI�N: 
 *   Este fichero contiene la clase ServerWorker, que es la encargada
 *   de ofrecer los m�todos remotos de c�lculo de n�meros primos.
 */
package ssdd.p2.assignmentServer;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

import ssdd.p2.workerServer.Worker;

/**
 * Clase encargada de proporcionar el m�todo remoto para asignar 
 * servidores de c�lculo
 * 
 * @author Adrian Moreno
 * @version 1.0 30 Oct 2014
 */
public class WorkerFactoryServer implements WorkerFactory, Runnable {
	
	private String ipRegistro; //IP de la m�quina donde est� el registro RMI
	
	/**
	 * Crea una nueva instancia de la clase WorkerFactoryServer
	 * @param ipRegistro IP de la m�quina donde se encuentra el registro RMI
	 */
	public WorkerFactoryServer(String ipRegistro) {
		this.ipRegistro = ipRegistro;
	}
	
	@Override
	public ArrayList<Worker> dameWorkers(int n) {
		try {
			
			System.out.println("Requesting workers");
			
			Registry registry = LocateRegistry.getRegistry(ipRegistro);
			ArrayList<Worker> workers = new ArrayList<Worker>();
			//Obtenci�n lista de todos los nombres registrados en el registro RMI
			String [] registeredObjects = registry.list();
			
			for (int i = 0; (i < n) && (i<registeredObjects.length); i++) {
				if (!registeredObjects[i].equals(REGISTRY_NAME)) {
					/*
					 * Se obtienen los objetos del registro a partir de su nombre
					 * si su nombre no coincide con el de esta clase.
					 * 
					 * XXX: Como haya mas clases registradas a parte de WorkerFactory y Worker, esto petar� 
					 */
					workers.add((Worker) registry.lookup(registeredObjects[i]));
				}
			}
			
			System.out.println("Workers list:");
			for (Worker w: workers) {
				System.out.println(w.toString());
			}
			
			System.out.println("End of request");
			return workers;
			
		} catch (RemoteException e) {
			e.printStackTrace();
			return null;
		} catch (NotBoundException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Este m�todo contiene el comportamiento del WorkerFactoryServer,
	 * para ser ejecutado en un thread independiente o directamente 
	 * desde el thread principal
	 */
	@Override
	public void run() {
		try {	
			
			System.out.println("Launching WorkerFactoryServer");
			
			//Creaci�n del objeto remoto
			WorkerFactory stub = (WorkerFactory) UnicastRemoteObject.exportObject(this, 0);
			//Obtenci�n del registro RMI
			Registry registry = LocateRegistry.getRegistry(ipRegistro);
			
			System.out.println("Attempting to bind...");
			
			//Registro en el registro RMI
			registry.bind(REGISTRY_NAME, stub);
			
			System.out.println("Successful binding");
			
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (AlreadyBoundException e) {
			e.printStackTrace();
		}
	}
}

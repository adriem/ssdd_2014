/*
 * AUTOR: Adri�n Moreno Jimeno
 * NIA: 631537
 * FICHERO: WorkerFactory.java
 * TIEMPO: 5 minutos
 * DESCRIPCI�N: 
 *   Este fichero contiene la interfaz WorkerFactory, que debe ser
 *   implementada por los objetos servidores de asignaci�n.
 */
package ssdd.p2.assignmentServer;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import ssdd.p2.workerServer.Worker;

/**
 * Interfaz que deben implementar los objetos servidores de asignaci�n
 * 
 * @author Adrian Moreno
 * @version 1.0 30 Oct 2014
 */
public interface WorkerFactory extends Remote {
	
	/** Nombre de este servidor en el registro RMI */
	public final static String REGISTRY_NAME = "WORKER_FACTORY"; 
	
	/**
	 * Devuelve un ArrayList con n referencias a Worker. Si hay menos de n 
	 * Workers registrados, devolver� tantas referencias a Worker como haya
	 * registradas.
	 * @param n N�mero de workers solicitados
	 * @return Un vector de hasta n referencias a Worker
	 */
	ArrayList<Worker> dameWorkers(int n) throws RemoteException;
	
}

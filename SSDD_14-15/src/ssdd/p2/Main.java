/*
 * AUTOR: Adri�n Moreno Jimeno
 * NIA: 631537
 * FICHERO: ServerWorker.java
 * TIEMPO: 15 minutos
 * DESCRIPCI�N: 
 *   Este fichero contiene la clase Main, que es la que
 *   contiene el m�todo main del programa.
 */
package ssdd.p2;

import ssdd.p2.assignmentServer.WorkerFactoryServer;
import ssdd.p2.client.Client;
import ssdd.p2.workerServer.ServerWorker;

/**
 * Clase contenedora del metodo main.
 * 
 * @author Adrian Moreno
 * @version 1.0 30 Oct 2014
 */
public class Main {
	public static void main(String[] args) {
		try {
			if (args[0].equals("-c")) {
				ServerWorker worker = new ServerWorker(args[1]);
				worker.run();
			} else if (args[0].equals("-a")) {
				WorkerFactoryServer factory = new WorkerFactoryServer(args[1]);
				factory.run();
			} else if (args[0].equals("-u")) {
				Client client = new Client(args[4], Integer.parseInt(args[3]),
						Integer.parseInt(args[1]),Integer.parseInt(args[2]));
				client.run();
			} else {
				System.out.println("Usage: java -jar SSDDp2.jar "
						+ "-c|-a|(-u min max n) IP_registro");
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Usage: java -jar SSDDp2.jar "
					+ "-c|-a|(-u min max n) IP_registro");
		}
	}
}

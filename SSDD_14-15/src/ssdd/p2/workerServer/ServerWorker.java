/*
 * AUTOR: Adri�n Moreno Jimeno
 * NIA: 631537
 * FICHERO: ServerWorker.java
 * TIEMPO: 3 horas
 * DESCRIPCI�N: 
 *   Este fichero contiene la clase ServerWorker, que es la encargada
 *   de ofrecer los m�todos remotos de c�lculo de n�meros primos.
 */
package ssdd.p2.workerServer;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Clase encargada de realizar el trabajo de del servidor de c�lculo.
 * 
 * @author Adrian Moreno
 * @version 1.0 30 Oct 2014
 */
public class ServerWorker implements Worker, Runnable {

	//L�mite superior del conjunto precalculado de primos
	private final static int FIRST_PRIMES_MAX = 997;		/* 997 es primo */
	//Conjunto precalculado con todos los primos menores que FIRST_PRIMES_MAX
	private final static ArrayList<Integer> FIRST_PRIMES = 
			new ArrayList<Integer>();
	
	private String ipRegistro; //IP de la m�quina donde est� el registro RMI
	
	/**
	 * Crea una nueva instancia de la clase ServerWorker
	 * @param ipRegistro IP de la m�quina donde se encuentra el registro RMI
	 */
	public ServerWorker(String ipRegistro) {
		this.ipRegistro = ipRegistro;
		/* Criba de Erathostenes */
		//Numeros naturales en [1,max]
		System.out.println("Precalculando los primeros " 
				+ FIRST_PRIMES_MAX + "primos");
		boolean[] marcados = new boolean[FIRST_PRIMES_MAX];
		for (int i = 2; i * i < FIRST_PRIMES_MAX; i++) {
			if (!marcados[i - 1]) {
				for (int j = i; j < FIRST_PRIMES_MAX/i; j++) {
					marcados[i * j - 1] = true;
				}
			}
		}
		//Guardamos en el array los primos en el intervalo
		for (int i = 1; i <= FIRST_PRIMES_MAX; i++) {
			if (!marcados[i - 1]) {
				FIRST_PRIMES.add(i);
				System.out.println(i);
			}
		}
	}
	
	@Override
	public ArrayList<Integer> encuentraPrimos(int min, int max) {
		
		System.out.println("Primes request [" + min + ", " + max + "]");
		
		ArrayList<Integer> primes = new ArrayList<Integer>();
		if (min > max) {
			primes = null;	//return null
		} else if (min < FIRST_PRIMES_MAX) {
			/*
			 * Si el intervalo comprende alguno de los primos precalculados,
			 * los a�adimos directamente
			 */
			Iterator<Integer> iterator = FIRST_PRIMES.iterator();
			int n = 0;
			while (iterator.hasNext() && (n <= max)) {
				n = iterator.next();
				if (n >= min && n <= max) {
					primes.add(n);
				}
			}
			/*
			 * Si los primos precalculados no cubren todo el rango que
			 * queremos analizar, a�adimos los dem�s calcul�ndolos
			 */
			if (n < max) {
				primes.addAll(encuentraPrimos(n + 1, max));
			}
		} else {
			//Recorremos uno a uno los numeros buscando sus divisores. 
			for (int i = min; i <= max; i++) {
				boolean isPrime = true;
				int potentialDivisor = 2;
				//Comprobamos si alguno de los primos precalculados divide a i
				Iterator<Integer> iterator = FIRST_PRIMES.iterator();
				while (isPrime && iterator.hasNext()) {
					potentialDivisor = iterator.next();
					if ((potentialDivisor > 1) && 
							(i % potentialDivisor == 0)) {
						isPrime = false;
					}
				}
				//Comprobamos si tiene divisores por encima de FIRST_PRIMES_MAX
				iterator = primes.iterator();
				while (isPrime && potentialDivisor <= Math.sqrt(i)) {
					if (i % potentialDivisor == 0) {
						isPrime = false;
					} else if (potentialDivisor >= min && iterator.hasNext()) {
						//No volvemos a pasar por aquellos candidatos no-primos
						potentialDivisor = iterator.next();
					} else {
						potentialDivisor++;
					}
				}
				if (isPrime) {
					primes.add(i);
				}
			}
		}
		
		System.out.println("Primes list:");
		for (Integer i: primes) {
			System.out.println(i);
		}
		
		System.out.println("End of request");
		return primes;
	}
	
	/**
	 * Este m�todo contiene el comportamiento de un ServerWorker, 
	 * para ser  ejecutado en un thread independiente o directamente 
	 * desde el thread principal
	 */
	@Override
	public void run() {
		try {	
			
			System.out.println("Launching ServerWorker");
			
			//Creaci�n del objeto remoto
			Worker stub = (Worker) UnicastRemoteObject.exportObject(this, 0);
			//Obtenci�n del registro RMI
			Registry registry = LocateRegistry.getRegistry(ipRegistro);
			
			System.out.println("Attempting to bind...");
			
			//Registro en el registro RMI
			//XXX: No es la mejor manera de evitar problemas de concurrencia
			boolean registrationSuccess = false;
			while (!registrationSuccess) {
				try{
					registry.bind(generateName(registry.list()), stub);
					registrationSuccess = true;
				} catch (AlreadyBoundException e) {}
			}
			
			System.out.println("Successful binding");
			
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Crea una nueva instancia de la clase ServerWorker
	 * @param ipRegistro IP de la m�quina donde se encuentra el registro RMI
	 * @return Un nombre que no se encuentra en currentNamesList
	 */  
	private static String generateName(String [] currentNamesList) {
		// XXX: Este m�todo puede presentar problemas de concurrencia
		int index = 0; 
		String name = REGISTRY_NAME + index;
		for (int i = 0; i < currentNamesList.length; i++) {
			if (name.equals(currentNamesList[i])) {
				index++;
				name = REGISTRY_NAME + index;
			}
		}
		return name;
	}
}

/*
 * AUTOR: Adri�n Moreno Jimeno
 * NIA: 631537
 * FICHERO: Worker.java
 * TIEMPO: 10 minutos
 * DESCRIPCI�N: 
 *   Este fichero contiene la interfaz worker, que debe ser
 *   implementada por los objetos servidores de calculo.
 */
package ssdd.p2.workerServer;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * Interfaz que deben implementar los objetos servidores de c�lculo
 * 
 * @author Adrian Moreno
 * @version 1.0 30 Oct 2014
 */
public interface Worker extends Remote {
	
	/** Codigo para generar el nombre en el registro RMI */

	final static String REGISTRY_NAME = "WORKER";
	
	/**
	 * Devuelve un ArrayList de enteros con todos los primos
	 * encontrados en el intervalo [min, max]
	 * @param min L�mite inferior del intervalo de enteros a analizar
	 * @param max L�mite superior del intervalo de enteros a analizar
	 * @return El conjunto de primos encontrados en el intervalo [min, max]
	 */
	ArrayList<Integer> encuentraPrimos(int min, int max) throws RemoteException;
	
}

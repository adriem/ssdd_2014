/*
 * AUTOR: Adri�n Moreno Jimeno
 * NIA: 631537
 * FICHERO: ServerWorker.java
 * TIEMPO: 3 horas
 * DESCRIPCI�N: 
 *   Este fichero contiene la clase ServerWorker, que es la encargada
 *   de ofrecer los m�todos remotos de c�lculo de n�meros primos.
 */
package ssdd.p2.client;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.concurrent.Semaphore;

import ssdd.p2.assignmentServer.WorkerFactory;
import ssdd.p2.workerServer.Worker;

/**
 * Clase encargada de realizar el trabajo del cliente.
 * 
 * @author Adrian Moreno
 * @version 1.0 30 Oct 2014
 */
public class Client implements Runnable {
	
	private String	ipRegistro;	//IP de la m�quina donde est� el registro RMI
	private int		n;			//N�mero de servidores a utilizar 
	private int		min;		//L�mite inferior del intervalo a analizar
	private int		max;		//L�mite superior del intervalo a analizar
	
	/**
	 * Crea una nueva instancia de la clase ServerWorker
	 * @param ipRegistro IP de la m�quina donde se encuentra el registro RMI
	 * @param n N�mero de servidores a utilizar
	 * @param min L�mite inferior del intervalo a analizar
	 * @param max L�mite superior del intervalo a analizar
	 */
	public Client(String ipRegistro, int n, int min, int max) {
		this.ipRegistro = ipRegistro;
		this.n = n;
		this.min = min;
		this.max = max;
	}
	
	/**
	 * Este m�todo contiene el comportamiento del cliente, para ser ejecutado
	 * en un thread independiente o directamente desde el thread principal
	 */
	@Override
	public void run() {
		try {
			
			System.out.println("Launching client");
			
			//Obtenci�n del registro RMI
			Registry registry = LocateRegistry.getRegistry(ipRegistro);
			//Obtenci�n del objeto remoto WorkerFactoryServer
			System.out.println("Requesting WorkerFactoryServer");
			WorkerFactory workerFactory = (WorkerFactory) registry.lookup(
					WorkerFactory.REGISTRY_NAME);
			//Calculo del numero de longitud del intervalo
			int longitudIntervalo = max-min+1;
			//Correci�n del valor de n
			n = (longitudIntervalo < n)? longitudIntervalo : n;
			//Obtenci�n del array de servidores de c�lculo
			System.out.println("Requesting " + n + " ServerWorkers");
			ArrayList<Worker> workers = workerFactory.dameWorkers(n);
			if (workers.size() == 0) {
				System.out.print("No workers available");
			}
			//Array donde guardaremos los resultados
			ArrayList<Integer> primos = new ArrayList<Integer>();
			//Control de exclusi�n mutua sobre primos
			Semaphore mutex = new Semaphore(1);
			//Control de que todos los Agentes han terminado
			Semaphore ready = new Semaphore(0);
			
			//Repartimos el trabajo
			System.out.println("Distributing work...");
			int workerMin = max+1;	//M�nimo asignado a un worker
			int workerMax;			//M�ximo asignado a un worker
			for (int i = workers.size() - 1; i>=0; i--) {
				//Calculamos el reparto de trabajo
				float porcentajeDeCarga = (float)(1.0 / n);
				/*
				 * Repartimos la carga de trabajo de forma inversamente
				 * proporcional al tama�o de los n�meros de cada intervalo,
				 * de manera que los trabajadores que tengan que calcular los 
				 * primos m�s pesados tengan menos n�meros que calcular.
				 */
				if ((porcentajeDeCarga != 1) && ((n % 2 != 0) || (i < n / 2))) {
					porcentajeDeCarga += (n / 2 - i) * porcentajeDeCarga / n;
				} else if ((porcentajeDeCarga != 1)) {
					porcentajeDeCarga += 
							(n / 2 - i - 1) * porcentajeDeCarga / n;
				}

				//Calculamos el intervalo real
				workerMax = workerMin-1;
				/*
				 * Como el calculo de porcentajes es aproximado, ya que estamos
				 * trabajando con enteros, en el �ltimo intervalo nos 
				 * aseguramos de llegar justo hasta el final
				 */
				workerMin = (i == 0)? min : Math.round(
						workerMax - longitudIntervalo * porcentajeDeCarga);
				
				System.out.println("Proccess " + i + ": " + porcentajeDeCarga 
						* 100 + "% [" + workerMin + ", " + workerMax + "]");
				
				//Lanzamos la tarea
				new Thread(new ClientAgent(ready, mutex,workers.get(i),
						workerMin,workerMax,primos)).start();
			}

			
			System.out.println("Waiting for the work to finish");
			ready.acquire(workers.size());
			//Eliminamos elementos repetidos
			HashSet<Integer> hs = new HashSet<Integer>();
			hs.addAll(primos);
			primos.clear();
			primos.addAll(hs);
			//Ordenamos el array
			Collections.sort(primos);
			System.out.println("N�mero de primos en el intervalo [" 
					+ min + "," + max + "]: " + primos.size());
			System.out.println(primos.toString());
			
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Clase encargada de solicitar el trabajo a un servidor en concreto.
	 * 
	 * @author Adrian Moreno
	 * @version 1.0 30 Oct 2014
	 */
	private class ClientAgent implements Runnable {
		
		private Semaphore ready; //Indica que se ha completado la petici�n
		private Semaphore mutex; //Indica que se puede escribir en el array
		private Worker worker; //Worker al que este agente le hace la petici�n
		private int min; //L�mite inferior del intervalo de la petici�n
		private int max; //L�mite superior del intervalo de la petici�n
		private ArrayList<Integer> primos; //Array donde almacenar el resultado
		
		public ClientAgent(Semaphore ready, Semaphore mutex, Worker worker, 
						   int min, int max, ArrayList<Integer> primos) {
			this.ready = ready;
			this.mutex = mutex;
			this.worker = worker;
			this.min = min;
			this.max = max;
			this.primos = primos;
		}
		
		@Override
		public void run() {
			try{
				ArrayList<Integer> temp = worker.encuentraPrimos(min, max);
				mutex.acquire();
				primos.addAll(temp);
				mutex.release();
				ready.release();
			} catch (RemoteException | InterruptedException e) {
				e.printStackTrace();
			}
			
		}
	}
}

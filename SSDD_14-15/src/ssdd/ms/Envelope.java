/*
 * AUTOR: Adri�n Moreno Jimeno
 * NIA: 631537
 * FICHERO: MassageSystem.java
 * TIEMPO: 5 minutos
 * DESCRIPCI�N: 
 *   Este fichero contiene la clase Envelope, encargada de empaquetar la 
 *   informacion que queremos transmitir en los mensajes
 */
package ssdd.ms;

import java.io.Serializable;

/**
 * Clase encargada de encargada de empaquetar la 
 * informacion que queremos transmitir en los mensajes
 * 
 * @author Adrian Moreno
 * @version 1.0 27 Nov 2014
 */
public class Envelope implements Serializable {

	private static final long serialVersionUID = -2619733359598341240L;

	private int stamp;
	private int source;
	private int destination;
	private Serializable payload;
	
	public Envelope(int stp, int s, int d, Serializable p) {
		stamp = stp;
		source = s;
		destination = d;
		payload = p;
	}
	
	public int getStamp() { 
		return stamp; 
	}
	
	public int getSource() { 
		return source; 
	}
	
	public int getDestination() { 
		return destination; 
	}
	
	public Serializable getPayload() { 
		return payload; 
	}
}

/*
 * AUTOR: Adri�n Moreno Jimeno
 * NIA: 631537
 * FICHERO: MassageSystem.java
 * TIEMPO: 1 hora (gran parte de la cual se invirtio en intentar implementar 
 *         una soluci�n no bloqueante para un unico thread)
 * DESCRIPCI�N: 
 *   Este fichero contiene la clase MailboxThread, encargada de gestionar
 *   los mensajes de entrada de un MessageSystem.
 */
package ssdd.ms;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

/**
 * Clase encargada de gestionar la recepcion de los mensajes en el MessageSystem
 * 
 * @author Adrian Moreno
 * @version 1.0 27 Nov 2014
 */
class MailboxThread extends Thread {
	
	private MessageSystem context;	//MessageSystem al que pertenece el mailbox
	private ServerSocket sSocket;	//Socket que escucha las peticiones de envio
	private Socket cSocket;	//Almacenar� temporalmente conexiones con clientes
	
	/**
	 * Crea una instancia de MailboxThread perteneciente a 
	 * context que recibe los mensajes en el puerto port
	 * 
	 * @param context MessageSystem al que pertenece este MailboxThread
	 * @param port Puerto donde se reciben los mensajes
	 */
	public MailboxThread (MessageSystem context, int port) {
		this.context = context;
		try{
			sSocket = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void run(){
		context.log("Running mailbox thread");
		try {
			/*
			 * Al hacer accept() sobre un socket cerrado o cerrar
			 * el socket mientras est� bloqueado en un accept,
			 * salta una SocketException que nos sacara del bucle
			 */
			while (true) {
				cSocket = sSocket.accept();
				new Thread() {
					@Override
					public void run() {
						try {
							Socket mSocket = cSocket;
							ObjectInputStream ois = new ObjectInputStream(
									mSocket.getInputStream());
							Envelope msg = (Envelope) ois.readObject();
							context.log(String.format(
									"[%d,%d] - Message retrieved:"
									+ "%s",msg.getStamp(), msg.getSource(), 
									msg.getPayload().toString()));
							if (context.queue.add(msg)) {
								context.log(String.format(
										"[%d,%d] - Message added to queue",
										msg.getStamp(),msg.getSource()));
							} else {
								context.log(String.format(
										"[%d,%d] - Error (FULL QUEUE) - "
										+ "Message discarded",msg.getStamp(),
										msg.getSource()));
							}
							mSocket.close();
						} catch (IOException e) {
							e.printStackTrace();
						} catch (ClassNotFoundException e) {
							e.printStackTrace();
						}
					}
				}.start();
			}
		} catch(SocketException e) {
			context.log("Mailbox closed");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Para el proceso. Los mensajes que se
	 * estuvieran enviando ser�n recibidos igualmente.
	 */
	public void stopServer() {
		context.log("Closing mailbox");
		try{
			sSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}

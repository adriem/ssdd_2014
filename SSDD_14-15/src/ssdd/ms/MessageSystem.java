/*
 * AUTOR: Adri�n Moreno Jimeno
 * NIA: 631537
 * FICHERO: MassageSystem.java
 * TIEMPO: 4 horas
 * DESCRIPCI�N: 
 *   Este fichero contiene la clase MessageSystem, encargada de gestionar
 *   el buzon de cada proceso. Adem�s, contiene las subclases Queue, que es
 *   el monitor mediante el que los procesos que recogen los mensajes los
 *   intercambian con el proceso principal, y Debugger, que ofrece m�todos
 *   para impimir informaci�n sobre el MessageSystem y su proceso due�o.
 */
package ssdd.ms;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Clase que representa el buzon de cada proceso encargado de implementar un
 * env�o y recepcion de mensajes desacoplados temporalmente.
 * 
 * @author Adrian Moreno
 * @version 1.0 27 Nov 2014
 */
public class MessageSystem {
	
	MsgQueue queue;	//Cola donde se guardan los mensajes hasta que son leidos

	private int stamp;		//Estampilla que se debe enviar en los mensajes
	private int source;		//Direcci�n del proceso due�o de este MessageSystem
	private boolean debug;	//Flag que activa los mensajes de depuracion
	private MailboxThread mailbox;//Thread encargado de revisar el ServerSocket
	private ArrayList<String> addressMap;	//Asocia direcciones con ips
	private ArrayList<Integer> portMap;		//Asocia direcciones con puertos
	
	/**
	 * Crea una nueva instancia de la clase MessageSystem, inicializando
	 * los valores de esta y arrancando un thread encargado de gestionar la
	 * recogida de los mensajes que llegan desde los otros procesos.
	 * 
	 * @param source Direccion del proceso due�o del MessageSystem
	 * @param networkFile Fichero con las direcciones y los puertos de la red
	 * @param debug Flag de activacion de mensajes de depuracion
	 * @param mailboxSize numero de mensajes sin leer que se pueden almacenar
	 * @throws FileNotFoundException Si no se encuentra networkFile
	 */
	public MessageSystem(int source, String networkFile, boolean debug, 
			 int mailboxSize)throws FileNotFoundException { 
		
		this.source = source;
		stamp = 0;
		this.debug = debug;
		this.queue = new MsgQueue(mailboxSize);		
		addressMap = new ArrayList<String>();
		portMap = new ArrayList<Integer>();
		
		//Analizar networkFile
		Scanner s = new Scanner(new File(networkFile));
		while(s.hasNextLine()){
			String str = s.nextLine();
			addressMap.add(str.substring(0, str.indexOf(":")));
			portMap.add(Integer.parseInt(str.substring(str.indexOf(":")+1)));
		}
		s.close();	
			
		//Arrancar mailbox
		mailbox = new MailboxThread(this, portMap.get(source-1));
		mailbox.start();
		
	}
	
	/**
	 * Devuelve el numero total de procesos del sistema
	 * 
	 * @return Numero total de procesos del sistema
	 */
	public int getNumberOfProcesses() {
		return portMap.size();
	}
	
	/**
	 * Devuelve el numero del proceso propietario del sistema
	 * 
	 * @return Numero del proceso propietario del sistema
	 */
	public int getSource() {
		return source;
	}

	/**
	 * Devuelve el valor actual de la estampilla
	 * 
	 * @return Valor actual de la estampilla
	 */
	public int getStamp() {
		return stamp;
	}
	
	/**
	 * Imprime por la salida estandar el mensaje msg si est�n activados
	 * los mensajes de depuraci�n
	 * 
	 * @param msg Mensaje a imprimir
	 */
	public void log(String str) {
		if (debug) {
			System.out.println(str);
		}
	}

	/**
	 * Obtiene el primer mensaje de la cola de mensajes.
	 * 
	 * @return El primer mensaje de la cola o null si la cola est� vac�a
	 */
	public Envelope receive() {
		Envelope e = queue.remove();
		Math.max(stamp, e.getStamp());
		return e;
	}

	/**
	 * Env�a un mensaje al proceso identificado mediante dst, con mSource
	 * como direccion de remitente
	 * 
	 * @param dst Direccion del proceso desinto
	 * @param message Mensaje a entregar
	 */
	public void send(int dst, Serializable message) {
		auxSend(dst,message,++stamp);
	}
	
	/**
	 * Env�a un mensaje al todos los procesos conocidos, con source
	 * como direccion de remitente
	 * 
	 * @param message Mensaje a entregar
	 */
	public void sendMulticast(Serializable message) {
		++stamp;
		for (int i=1; i<=getNumberOfProcesses(); i++) {
			auxSend(i, message, stamp);
		}
	}
	
	/**
	 * Para el proceso que gestiona las escuchas del mailbox. Los mensajes
	 * que se estuvieran enviando ser�n recibidos igualmente.
	 */
	public void stopMailbox() {
		mailbox.stopServer();
	}
	
	/**
	 * M�todo privado a nivel de paquete que env�a un mensaje al proceso 
	 * identificado mediante dst, con mSource como direccion de remitente.
	 * Es el proceso sobre el que recae todo el trabajo de la codificaci�n.
	 * 
	 * @param dst Direccion del proceso desinto
	 * @param message Mensaje a entregar
	 * @param stamp Estampilla del mensaje a entregar
	 */
	void auxSend(int dst, Serializable message, int stamp) {
		Envelope envp = new Envelope(stamp, source, dst, message);
		try{
			Socket sock = new Socket(addressMap.get(dst-1), portMap.get(dst-1));
			new ObjectOutputStream(sock.getOutputStream()).writeObject(envp);
			this.log(String.format("[%d,%d] - Sent message to %d:%s", 
					stamp, source, dst, message.toString()));
			sock.close();
		} catch (IOException e) {
			this.log(String.format("[%d,%d] - Error (MESSAGE SENDING FAILURE)"
					+ " - Details:%n%s", stamp, source, e.getMessage()));
		}
	}
	
}
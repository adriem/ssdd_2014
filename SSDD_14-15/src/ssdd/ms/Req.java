/*
 * AUTOR: Adri�n Moreno Jimeno
 * NIA: 631537
 * FICHERO: MassageSystem.java
 * TIEMPO: 2 minutos
 * DESCRIPCI�N: 
 *   Este fichero contiene la clase Req, que representa un mensaje de 
 *   solicitud de acceso a la secci�n cr�tica
 */
package ssdd.ms;

import java.io.Serializable;

/**
 * Clase que representa un mensaje de solicitud de acceso a la secci�n cr�tica
 * 
 * @author Adrian Moreno
 * @version 1.0 27 Nov 2014
 */
public class Req implements Serializable {


	private static final long serialVersionUID = 6947776458788294940L;
	
	@Override
	public String toString() {
		return "REQ";
	}
	
}

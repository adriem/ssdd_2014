/*
 * AUTOR: Adri�n Moreno Jimeno
 * NIA: 631537
 * FICHERO: MsgQueue.java
 * TIEMPO: 10 minutos
 * DESCRIPCI�N: 
 *   Este fichero contiene la clase MsgQueue, que es una cola con implementaci�n
 *   est�tica circular en la que se van almacenando los mensajes a la espera de 
 *   ser leidos.
 */
package ssdd.ms;

/**
 * Clase que almacena los mensajes recibidos a la espera de ser leidos.
 * Esta clase es thread-safe, sus operaciones est�n sincronizadas.
 * 
 * @author Adrian Moreno
 * @version 1.0 13 Nov 2014
 */
class MsgQueue {
	
	private Envelope [] elements;	//Array donde se almacenan los elementos
	private int beginIndex;			//Posicion del primer elemento
	private int endIndex;			//Posicion siguiente del ultimo elemento
	private int size;				//Numero de elementos almacenados
	
	/**
	 * Crea una instancia de una MsgQueue vac�a, con tama�o igual a size
	 * 
	 * @param size Tama�o de la cola a crear
	 */
	public MsgQueue(int size) {
		elements = new Envelope[size];
		beginIndex = 0;
		endIndex = 0;
		size = 0;
	}
	
	/**
	 * Si hay sitio, a�ade el elemento newElement a la cola
	 * 
	 * @param newElement Nuevo elemento a introducir
	 * @return True si y solo si la insercion se realizo con exito
	 */
	public synchronized boolean add(Envelope newElement) {
		if (size == elements.length) {
			return false;
		} else {
			elements[endIndex] = newElement;
			endIndex = (endIndex + 1) % elements.length;
			size++;
			if (true) {
				notifyAll();
			}
			return true;
		}
	}
	
	/**
	 * Si est� activada la lectura bloqueante, devuelve el primer elemento
	 * de la cola y lo elimina si no esta vacia o, si lo esta, bloquea hasta
	 * que haya al menus un elemento. Si no esta activada la lectura 
	 * bloqueante, si la cola est� vac�a devuelve null
	 * 
	 * @return El primer elemento de la cola si no esta vacia o null 
	 */
	@SuppressWarnings("unused")
	public synchronized Envelope remove() {
		if(size == 0 && !true) {
			return null;
		} else {
			try{
				while(size == 0){
					wait();
				}
				size--;
				int aux = beginIndex;
				beginIndex = (beginIndex + 1) % elements.length;
				return elements[aux];
			} catch (InterruptedException e) {
				e.printStackTrace();
				return null;
			}
		}
	}

}
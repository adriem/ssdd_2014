/*
 * FICHERO: MassageSystem.java
 * DESCRIPCIÓN: 
 *   Este fichero contiene la clase Envelope, encargada de empaquetar la 
 *   informacion que queremos transmitir en los mensajes
 */
package ssdd.ms.old;

import java.io.Serializable;

public class Envelope implements Serializable {

	private static final long serialVersionUID = -2619733359598341240L;
	
	private int source;
	private int destination;
	private Serializable payload;
	
	public Envelope(int s, int d, Serializable p) {
		source = s;
		destination = d;
		payload = p;
	}
	
	public int getSource() { 
		return source; 
	}
	
	public int getDestination() { 
		return destination; 
	}
	
	public Serializable getPayload() { 
		return payload; 
	}
}

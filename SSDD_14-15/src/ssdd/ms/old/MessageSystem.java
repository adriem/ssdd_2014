/*
 * AUTOR: Adri�n Moreno Jimeno
 * NIA: 631537
 * FICHERO: MassageSystem.java
 * TIEMPO: 4 horas
 * DESCRIPCI�N: 
 *   Este fichero contiene la clase MessageSystem, encargada de gestionar
 *   el buzon de cada proceso. Adem�s, contiene las subclases Queue, que es
 *   el monitor mediante el que los procesos que recogen los mensajes los
 *   intercambian con el proceso principal, y Debugger, que ofrece m�todos
 *   para impimir informaci�n sobre el MessageSystem y su proceso due�o.
 */
package ssdd.ms.old;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Clase que representa el buzon de cada proceso encargado de implementar un
 * env�o y recepcion de mensajes desacoplados temporalmente.
 * 
 * @author Adrian Moreno
 * @version 1.0 13 Nov 2014
 */
public class MessageSystem {
	
	/** Tama�o m�ximo de mensajes sin leer en el buzon */
	public static final int MAILBOX_SIZE = 5;
	public final boolean BLOCK_ON_RECEIVE = true;
	
	Debugger debug;	//Debugger para este MessageSystem
	Queue queue;	//Cola donde se guardan los mensajes hasta que son leidos
	int source; 	//Direcci�n del proceso due�o de este MessageSystem
	
	private MailboxThread mailbox;//Thread encargado de revisar el ServerSocket
	private ArrayList<String> addressMap;	//Asocia direcciones con ips
	private ArrayList<Integer> portMap;		//Asocia direcciones con puertos
	
	/**
	 * Crea una nueva instancia de la clase MessageSystem, inicializando
	 * los valores de esta y arrancando un thread encargado de gestionar la
	 * recogida de los mensajes que llegan desde los otros procesos.
	 * 
	 * @param source Direccion del proceso due�o del MessageSystem
	 * @param networkFile Fichero con las direcciones y los puertos de la red
	 * @param debug Flag de activacion de mensajes de depuracion
	 * @throws FileNotFoundException Si no se encuentra networkFile
	 */
	public MessageSystem(int source, String networkFile, boolean debug) 
			throws FileNotFoundException { 
		
		this.source = source;
		this.debug = new Debugger(debug, source);
		this.queue = new Queue();		
		addressMap = new ArrayList<String>();
		portMap = new ArrayList<Integer>();
		
		//Analizar networkFile
		Scanner s = new Scanner(new File(networkFile));
		while(s.hasNextLine()){
			String str = s.nextLine();
			addressMap.add(str.substring(0, str.indexOf(":")));
			portMap.add(Integer.parseInt(str.substring(str.indexOf(":")+1)));
		}
		s.close();	
			
		//Arrancar mailbox
		mailbox = new MailboxThread(this, portMap.get(source-1));
		mailbox.start();
	}
	
	/**
	 * Env�a un mensaje al proceso identificado mediante dst, con mSource
	 * como direccion de remitente
	 * 
	 * @param dst Direccion del proceso desinto
	 * @param message Mensaje a entregar
	 */
	public void send(int dst, Serializable message) {
		Envelope envp = new Envelope(source, dst, message);
		try{
			debug.log("Establishing conection with " + dst);
			Socket sock = new Socket(addressMap.get(dst-1), portMap.get(dst-1));
			debug.log(String.format("Sending message%nTO: %d%nBODY%n%s", 
					   dst, message.toString()));
			new ObjectOutputStream(sock.getOutputStream()).writeObject(envp);
			debug.log("Message sent");
			sock.close();
			debug.log("End of transmission");
		} catch (IOException e) {
			debug.log(String.format("Error - Message sending failure - "
					+ "Details:%n%s", e.getMessage()));
		}
	}
	
	/**
	 * Obtiene el primer mensaje de la cola de mensajes.
	 * 
	 * @return El primer mensaje de la cola o null si la cola est� vac�a
	 */
	public Envelope receive() {
		return (Envelope) queue.remove();
	}
	
	/**
	 * Para el proceso que gestiona las escuchas del mailbox. Los mensajes
	 * que se estuvieran enviando ser�n recibidos igualmente.
	 */
	public void stopMailbox() {
		mailbox.stopServer();
	}
	
	
	/**
	 * Clase que almacena los mensajes recibidos a la espera de ser leidos.
	 * Esta clase es thread-safe, sus operaciones son restringidas.
	 * 
	 * @author Adrian Moreno
	 * @version 1.0 13 Nov 2014
	 */
	class Queue {
		
		private Serializable [] elements = new Serializable[MAILBOX_SIZE];
		private int beginIndex = 0;
		private int endIndex = 0;
		private int size = 0;
		
		/**
		 * Si hay sitio, a�ade el elemento newElement a la cola
		 * 
		 * @param newElement Nuevo elemento a introducir
		 * @return True si y solo si la insercion se realizo con exito
		 */
		public synchronized boolean add(Serializable newElement) {
			if (size == elements.length) {
				return false;
			} else {
				elements[endIndex] = newElement;
				endIndex = (endIndex + 1) % elements.length;
				size++;
				if (BLOCK_ON_RECEIVE) {
					notifyAll();
				}
				return true;
			}
		}
		
		/**
		 * Si est� activada la lectura bloqueante, devuelve el primer elemento
		 * de la cola y lo elimina si no esta vacia o, si lo esta, bloquea hasta
		 * que haya al menus un elemento. Si no esta activada la lectura 
		 * bloqueante, si la cola est� vac�a devuelve null
		 * 
		 * @return El primer elemento de la cola si no esta vacia o null 
		 */
		@SuppressWarnings("unused")
		public synchronized Serializable remove() {
			if(size == 0 && !BLOCK_ON_RECEIVE) {
				return null;
			} else {
				try{
					while(size == 0){
						wait();
					}
					size--;
					int aux = beginIndex;
					beginIndex = (beginIndex + 1) % elements.length;
					return elements[aux];
				} catch (InterruptedException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}
	
	
	/**
	 * Clase que permite a un proceso activar o desactivar la salida de
	 * mensajes de debug, as� como enviarlos.
	 * 
	 * @author Adrian Moreno
	 * @version 1.0 13 Nov 2014
	 */
	class Debugger {
		
		public boolean debug;	//Indica si estan activados los mensajes
		public int source;		//Indica el emisor de los mensajes
		
		/**
		 * Crea una instancia debugger con los par�metros debug y source
		 * 
		 * @param debug Indica si hay que imprimir los mensajes o no
		 * @param source Indica qu� proceso est� imprimiendo los mensajes
		 */
		public Debugger(boolean debug, int source) {
			this.debug = debug;
			this.source = source;
		}
		
		/**
		 * Imprime por la salida estandar el mensaje msg si est�n activados
		 * los mensajes de depuraci�n
		 * 
		 * @param msg Mensaje a imprimir
		 */
		public void log (String str) {
			if (debug) {
				System.out.printf("Process %d: %s%n", source, str);
			}
		}
		
		/**
		 * Imprime por la salida de error el mensaje msg si est�n activados
		 * los mensajes de depuraci�n
		 * 
		 * @param msg Mensaje a imprimir
		 */
		public void error (String str) {
			if (debug) {
				System.out.printf("Process %d: %s%n", source, str);
			}
		}
	}
}
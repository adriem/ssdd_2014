/*
 * AUTOR: Adri�n Moreno Jimeno
 * NIA: 631537
 * FICHERO: TotalOrderMulticast.java
 * TIEMPO: 3 horas
 * DESCRIPCI�N: 
 *   Este fichero contiene la clase TotalOrderMulticast, que es la encargada de
 *   controla el acceso a la secci�n cr�tica mediante el algoritmo distribuido 
 *   de Ricart y Agrawala.
 */
package ssdd.ms;

import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.concurrent.Semaphore;

/**
 * Clase que controla el acceso a la secci�n cr�tica
 * mediante el algoritmo distribuido de Ricart y Agrawala
 * 
 * @author Adrian Moreno
 * @version 1.0 27 Nov 2014
 */
public class TotalOrderMulticast {

	private MessageSystem ms;

	private boolean requestCS;	//Flag que indica si queremos acceder a la SC
	private Semaphore pendientes;
	private int[] replyDeferred;

	/**
	 * Crea una nueva instancia de la clase TotalOrderMulticast, inicializando
	 * los valores de esta.
	 * 
	 * @param ms MessageSystem mediante el que se enviar�n los mensajes
	 */
	public TotalOrderMulticast(MessageSystem ms) {
		this.ms = ms;
		pendientes = new Semaphore(0);
		replyDeferred = new int[ms.getNumberOfProcesses()];
		for (int i=0; i<replyDeferred.length; i++) {
			replyDeferred[i] = -1; //-1 indica que no hay que mandar nada
		}
	}
	
	/**
	 * Envia un mensaje a todos los procesos, acediendo previamente a la 
	 * secci�n cr�tica
	 * 
	 * @param message Mensaje a enviar
	 */
	public void sendMulticast(Serializable message) {
		requestCS = true;
		int stamp = ms.getStamp() + 1; //Estampilla del mensaje que enviaremos
		//Enviamos peticiones REQ
		for (int i=1; i<=ms.getNumberOfProcesses(); i++) {
			if(i != ms.getSource()) {
				ms.auxSend(i, new Req(), stamp);
			}
		}
		//Esperamos a que nos contesten
		try {
			pendientes.acquire(ms.getNumberOfProcesses()-1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//Enviamos el mensaje real
		for (int i=1; i<=ms.getNumberOfProcesses(); i++) {
			ms.auxSend(i, message, stamp);
		}
		//Salimos de seccion critica
		requestCS = false;
		for (int i = 0; i < replyDeferred.length; i++) {
			if (replyDeferred[i] != -1) {
				ms.auxSend(i+1, new Ack(), replyDeferred[i]);
			}
		}
	}
	
	/**
	 * Recibe un mensaje de cualquier proceso, gestionando autom�ticamente
	 * las peticiones y confirmaciones de acceso a la seccion critica que
	 * se reciben por otros procesos.
	 * 
	 * @return Mensaje recibido
	 */
	public Envelope receiveMulticast() {
		while(true) {
			Envelope e = ms.receive();
			//Si recibimos una peticion REQ
			if (e.getPayload().getClass().getName().equals("ssdd.ms.Req")) {
				if (requestCS && (ms.getStamp() < e.getStamp() ||
						(ms.getStamp() == e.getStamp() && 
						ms.getSource() < e.getSource()))) {
					replyDeferred[e.getSource()-1] = e.getStamp();
				} else {
					ms.auxSend(e.getSource(), new Ack(), e.getStamp());
				}
			//Si recibimos una contestacion ACK
			} else if (e.getPayload().getClass().getName().equals("ssdd.ms.Ack")) {
				pendientes.release();
			//Si recibimos un mensaje
			} else {
				return e;
			}
		}
	}
	
}